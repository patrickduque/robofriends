import React, { Component } from 'react';
import Cardlist from './Cardlist';
import Searchbox from './Searchbox';
import { robots } from './robots';
import './App.css';
 

class App extends Component {
    constructor(){
        super()
        this.state = {
            robots: [],
            searchfield: ''
        }
    }

    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users').then(response => response.json()).then(user => this.setState({ robots: robots }))
    }

    onSearchChange = (event) => {
        this.setState({ searchfield: event.target.value })
    }
    render () {
        const filteredRobots = this.state.robots.filter(robots => {
            return robots.name.toLowerCase().includes(this.state.searchfield.toLowerCase());
        })
        return (
            <div className='tc'>
                <h1 className='f2'>RoboFriends</h1>
                <Searchbox searchChange={this.onSearchChange}/>
                <div>
                    <Cardlist robots={filteredRobots} />
                </div>
            </div>
        )
    }    
}

export default App;